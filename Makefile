CC := arm-linux-gnueabihf-g++
CFLAGS := -g -Wall
LDFLAGS :=

ifdef SYSROOT
  CFLAGS+=--sysroot=$(SYSROOT)
endif

CFLAGS+=-I$(SYSROOT)/opt/vc/include -I$(SYSROOT)/opt/vc/include/interface/vmcs_host/linux/ -I$(SYSROOT)/opt/vc/include/interface/vcos/pthreads/ -I$(SYSROOT)/usr/include/cairo
CFLAGS+=-DOMX_SKIP64BIT
LDFLAGS+=-L$(SYSROOT)/opt/vc/lib -lvcos -lbcm_host -lvchiq_arm -lopenmaxil -lEGL -lGLESv2 -lcairo

omximagedecode: omximagedecode.c
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@

all: omximagedecode

clean:
	rm -f omximagedecode

.PHONY: clean all
