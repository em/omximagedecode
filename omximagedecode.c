#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <assert.h>
#include <cairo.h>

#include <bcm_host.h>
#include <IL/OMX_Core.h>
#include "IL/OMX_Component.h"
#include "IL/OMX_Broadcom.h"

#define MAX(a,b) (((a)>(b))?(a):(b))

#define MAX_BUFFERS 8
#define MAX_DECODED_DATA 8192*8192*4

typedef enum {
    BUFFER_UNUSED,
    BUFFER_USED
} BufferStatus;

// list of events we block on
enum {
    EVENT_PORT_DISABLE          = 1 << 1,
    EVENT_PORT_ENABLE           = 1 << 2,
    EVENT_STATE_SET             = 1 << 3,
    EVENT_PORT_SETTINGS_CHANGED = 1 << 4,
    EVENT_BUFFER_FLAG           = 1 << 5,
    EVENT_BUFFER_FILLED         = 1 << 6
};

static pthread_mutex_t event_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t  event_cond;

static uint32_t events;

uint8_t decoded_data[MAX_DECODED_DATA] = { 0 };
size_t decoded_data_offset = 0;

const char* omxerrorstr(OMX_ERRORTYPE err)
{
    switch (err) {
        case OMX_ErrorNone:
            return "None";
        case OMX_ErrorInsufficientResources:
            return "Insufficient resources";
        case OMX_ErrorUndefined:
            return "Undefined";
        case OMX_ErrorInvalidComponentName:
            return "Invalid component name";
        case OMX_ErrorComponentNotFound:
            return "Component not found";
        case OMX_ErrorInvalidComponent:
            return "Invalid component";
        case OMX_ErrorBadParameter:
            return "Bad parameter";
        case OMX_ErrorNotImplemented:
            return "Not implemented";
        case OMX_ErrorUnderflow:
            return "Underflow";
        case OMX_ErrorOverflow:
            return "Overflow";
        case OMX_ErrorHardware:
            return "Hardware";
        case OMX_ErrorInvalidState:
            return "Invalid state";
        case OMX_ErrorStreamCorrupt:
            return "Stream corrupt";
        case OMX_ErrorPortsNotCompatible:
            return "Ports not compatible";
        case OMX_ErrorResourcesLost:
            return "Resources lost";
        case OMX_ErrorNoMore:
            return "No more";
        case OMX_ErrorVersionMismatch:
            return "Version mismatch";
        case OMX_ErrorNotReady:
            return "Not ready";
        case OMX_ErrorTimeout:
            return "Timeout";
        case OMX_ErrorSameState:
            return "Same state";
        case OMX_ErrorResourcesPreempted:
            return "Resources preempted";
        case OMX_ErrorPortUnresponsiveDuringAllocation:
            return "Port unresponsive during allocation";
        case OMX_ErrorPortUnresponsiveDuringDeallocation:
            return "Port unresponsive during deallocation";
        case OMX_ErrorPortUnresponsiveDuringStop:
            return "Port unresponsive during stop";
        case OMX_ErrorIncorrectStateTransition:
            return "Incorrect state transition";
        case OMX_ErrorIncorrectStateOperation:
            return "Incorrect state operation";
        case OMX_ErrorUnsupportedSetting:
            return "Unsupported setting";
        case OMX_ErrorUnsupportedIndex:
            return "Unsupported index";
        case OMX_ErrorBadPortIndex:
            return "Bad port index";
        case OMX_ErrorPortUnpopulated:
            return "Port unpopulated";
        case OMX_ErrorComponentSuspended:
            return "Component suspended";
        case OMX_ErrorDynamicResourcesUnavailable:
            return "Dynamic resources unavailable";
        case OMX_ErrorMbErrorsInFrame:
            return "Macroblock errors in frame";
        case OMX_ErrorFormatNotDetected:
            return "Format not detected";
        case OMX_ErrorContentPipeOpenFailed:
            return "Content pipe open failed";
        case OMX_ErrorContentPipeCreationFailed:
            return "Content pipe creation failed";
        case OMX_ErrorSeperateTablesUsed:
            return "Separate tables used";
        case OMX_ErrorTunnelingUnsupported:
            return "Tunneling unsupported";
        default:
            return "Unknown error";
    }
}

const char* omxstatestr(OMX_STATETYPE state)
{
    switch (state) {
        case OMX_StateLoaded:
            return "loaded";
        case OMX_StateIdle:
            return "idle";
        case OMX_StateExecuting:
            return "executing";
        default:
            return "Unknown";
    }
}

OMX_ERRORTYPE event_handler(OMX_IN OMX_HANDLETYPE hComponent,
      OMX_IN OMX_PTR pAppData,
      OMX_IN OMX_EVENTTYPE eEvent,
      OMX_IN OMX_U32 nData1,
      OMX_IN OMX_U32 nData2,
      OMX_IN OMX_PTR pEventData)
{
    pthread_mutex_lock(&event_lock);
    uint32_t event = 0;
    printf("%s:%d unacknowledged events:0x%x\n", __func__, __LINE__, events);
    switch (eEvent) {
        case OMX_EventCmdComplete:
            switch(nData1) {
                case OMX_CommandStateSet:
                    event = EVENT_STATE_SET;
                    printf("%s:%d command state set to %s\n", __func__, __LINE__, omxstatestr((OMX_STATETYPE)nData2));
                    break;
                case OMX_CommandFlush:
                    printf("%s:%d command flush %d\n", __func__, __LINE__, nData2);
                    break;
                case OMX_CommandPortDisable:
                    event = EVENT_PORT_DISABLE;
                    printf("%s:%d command port disable on %d\n", __func__, __LINE__, nData2);
                    break;
                case OMX_CommandPortEnable:
                    event = EVENT_PORT_ENABLE;
                    printf("%s:%d command port enable on %d\n", __func__, __LINE__, nData2);
                    break;
                case OMX_CommandMarkBuffer:
                    printf("%s:%d command mark buffer %d\n", __func__, __LINE__, nData2);
                    break;
                case OMX_CommandMax:
                    printf("%s:%d command max %d\n", __func__, __LINE__, nData2);
                    break;
                default:
                    printf("%s:%d unknown command %d %d\n", __func__, __LINE__, nData1, nData2);
            }
            break;
        case OMX_EventPortSettingsChanged:
            event = EVENT_PORT_SETTINGS_CHANGED;
            printf("%s:%d settings changed on %d\n", __func__, __LINE__, nData1);
            break;
        case OMX_EventBufferFlag:
            event = EVENT_BUFFER_FLAG;
            printf("%s:%d buffer flag %d\n", __func__, __LINE__, nData1);
            break;
        case OMX_EventError:
            printf("%s:%d error %s\n", __func__, __LINE__, omxerrorstr((OMX_ERRORTYPE)nData1));
            exit(1);
        default:
            printf("%s:%d unknown event %d %d\n", __func__, __LINE__, nData1, nData2);
    }
    events |= event;
    pthread_cond_signal(&event_cond);
    pthread_mutex_unlock(&event_lock);
    return OMX_ErrorNone;
}

static int test_event(uint32_t event)
{
    int test = 0;
    printf("%s:%d\n", __func__, __LINE__);
    pthread_mutex_lock(&event_lock);
    if (events & event)
    {
        events &= ~event;
        test = 1;
    }
    pthread_mutex_unlock(&event_lock);
    return test;
}

OMX_ERRORTYPE empty_buffer_done(OMX_OUT OMX_HANDLETYPE hComponent,
                                OMX_OUT OMX_PTR pAppData,
                                OMX_OUT OMX_BUFFERHEADERTYPE* pBuffer)
{
    printf("%s:%d buffer %p\n", __func__, __LINE__, pBuffer);

    pthread_mutex_lock(&event_lock);
    pBuffer->pAppPrivate = (OMX_PTR) BUFFER_UNUSED;
    pthread_cond_signal(&event_cond);
    pthread_mutex_unlock(&event_lock);
    return OMX_ErrorNone;
}

static OMX_BUFFERHEADERTYPE *get_free_buffer(OMX_BUFFERHEADERTYPE* buffers[])
{
  OMX_BUFFERHEADERTYPE *buffer = NULL;
  printf("%s:%d\n", __func__, __LINE__);
  pthread_mutex_lock(&event_lock);
  printf("%s:%d\n", __func__, __LINE__);

  for (int i=0; i<MAX_BUFFERS; i++) {
      OMX_BUFFERHEADERTYPE *b = buffers[i];
      if (b != NULL && b->pAppPrivate == (OMX_PTR) BUFFER_UNUSED) {
          buffer = b;
          buffer->pAppPrivate = (OMX_PTR) BUFFER_USED;
          break;
      }
  }

  pthread_mutex_unlock(&event_lock);
  return buffer;
}

OMX_ERRORTYPE fill_buffer_done(OMX_OUT OMX_HANDLETYPE hComponent,
      OMX_OUT OMX_PTR pAppData,
      OMX_OUT OMX_BUFFERHEADERTYPE* pBuffer)
{
    printf("%s:%d buffer %p len %d/%d offset:%u flags:%x\n", __func__, __LINE__, pBuffer, pBuffer->nFilledLen, pBuffer->nAllocLen, decoded_data_offset, pBuffer->nFlags);

    pthread_mutex_lock(&event_lock);
    events |= EVENT_BUFFER_FILLED;

    assert(decoded_data_offset + pBuffer->nFilledLen < MAX_DECODED_DATA);

    memcpy(decoded_data + decoded_data_offset, pBuffer->pBuffer, pBuffer->nFilledLen);
    decoded_data_offset += pBuffer->nFilledLen;

    pBuffer->pAppPrivate = (OMX_PTR) BUFFER_UNUSED;

    pthread_cond_signal(&event_cond);
    pthread_mutex_unlock(&event_lock);

    return OMX_ErrorNone;
}

static void component_init_handle(OMX_HANDLETYPE *handle, char *identifier, OMX_CALLBACKTYPE *callbacks)
{
    printf("%s:%d\n", __func__, __LINE__);
    OMX_ERRORTYPE result;
    result = OMX_GetHandle(handle, identifier, NULL, callbacks);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
}

static void component_get_ports(OMX_HANDLETYPE handle, int *inPort, int *outPort)
{
    printf("%s:%d\n", __func__, __LINE__);
    OMX_ERRORTYPE result;
    OMX_PORT_PARAM_TYPE port = OMX_PORT_PARAM_TYPE();
    port.nSize = sizeof(OMX_PORT_PARAM_TYPE);
    port.nVersion.nVersion = OMX_VERSION;
    result = OMX_GetParameter(handle, OMX_IndexParamImageInit, &port);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));

    *inPort = port.nStartPortNumber;
    *outPort = port.nStartPortNumber + 1;
}

static void component_disable_port(OMX_HANDLETYPE handle, int port)
{
    printf("%s:%d\n", __func__, __LINE__);
    OMX_ERRORTYPE result;
    result = OMX_SendCommand(handle, OMX_CommandPortDisable, port, NULL);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
}

static void component_enable_port(OMX_HANDLETYPE handle, int port)
{
    printf("%s:%d\n", __func__, __LINE__);
    OMX_ERRORTYPE result;
    result = OMX_SendCommand(handle, OMX_CommandPortEnable, port, NULL);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
}

static void component_set_state(OMX_HANDLETYPE handle, OMX_STATETYPE state)
{
    printf("%s:%d\n", __func__, __LINE__);
    OMX_ERRORTYPE result;
    result = OMX_SendCommand(handle, OMX_CommandStateSet, state, NULL);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
}

static void component_setup_input_port(OMX_HANDLETYPE handle, int port)
{
    printf("%s:%d\n", __func__, __LINE__);
    OMX_ERRORTYPE result;
    OMX_IMAGE_PARAM_PORTFORMATTYPE inPortFormat = OMX_IMAGE_PARAM_PORTFORMATTYPE();
    inPortFormat.nSize = sizeof(OMX_IMAGE_PARAM_PORTFORMATTYPE);
    inPortFormat.nVersion.nVersion = OMX_VERSION;
    inPortFormat.nPortIndex = port;
    inPortFormat.eCompressionFormat = OMX_IMAGE_CodingJPEG;
    result = OMX_SetParameter(handle, OMX_IndexParamImagePortFormat, &inPortFormat);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
}

static void component_setup_output_port(OMX_HANDLETYPE handle, int port)
{
    printf("%s:%d\n", __func__, __LINE__);
    OMX_ERRORTYPE result;
    OMX_IMAGE_PARAM_PORTFORMATTYPE outPortFormat = OMX_IMAGE_PARAM_PORTFORMATTYPE();
    outPortFormat.nSize = sizeof(OMX_IMAGE_PARAM_PORTFORMATTYPE);
    outPortFormat.nVersion.nVersion = OMX_VERSION;
    outPortFormat.nPortIndex = port;
    outPortFormat.eColorFormat = OMX_COLOR_Format32bitARGB8888;
    outPortFormat.eCompressionFormat = OMX_IMAGE_CodingUnused;
    result = OMX_SetParameter(handle, OMX_IndexParamImagePortFormat, &outPortFormat);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
}

static void component_get_buffering_parameters(OMX_HANDLETYPE handle, int port, int *buffersCount, int *buffersSize, int *stride, int *slice)
{
    printf("%s:%d\n", __func__, __LINE__);
    OMX_ERRORTYPE result;
    OMX_PARAM_PORTDEFINITIONTYPE portDefinition = OMX_PARAM_PORTDEFINITIONTYPE();
    portDefinition.nSize = sizeof(OMX_PARAM_PORTDEFINITIONTYPE);
    portDefinition.nVersion.nVersion = OMX_VERSION;
    portDefinition.nPortIndex = port;
    result = OMX_GetParameter(handle, OMX_IndexParamPortDefinition, &portDefinition);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));

    *buffersCount = portDefinition.nBufferCountActual;
    *buffersSize = portDefinition.nBufferSize;
    if (stride)
        *stride = portDefinition.format.image.nStride;
    if (slice)
        *slice = portDefinition.format.image.nSliceHeight;

    printf("%s:%d count:%d size:%d\n", __func__, __LINE__,  portDefinition.nBufferCountActual, portDefinition.nBufferSize);
    printf("%s:%d stride:%d slice:%d\n", __func__, __LINE__, portDefinition.format.image.nStride, portDefinition.format.image.nSliceHeight);
}

static void component_set_buffering_parameters(OMX_HANDLETYPE handle, int port, int buffersCount, int buffersSize, int stride, int slice)
{
    OMX_ERRORTYPE result;
    OMX_PARAM_PORTDEFINITIONTYPE portDefinition = OMX_PARAM_PORTDEFINITIONTYPE();
    portDefinition.nSize = sizeof(OMX_PARAM_PORTDEFINITIONTYPE);
    portDefinition.nVersion.nVersion = OMX_VERSION;
    portDefinition.nPortIndex = port;
    result = OMX_GetParameter(handle, OMX_IndexParamPortDefinition, &portDefinition);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));

    portDefinition.nBufferCountActual = buffersCount;
    portDefinition.nBufferSize = buffersSize;
    portDefinition.format.image.nStride = stride;
    portDefinition.format.image.nSliceHeight = slice;

    result = OMX_SetParameter(handle, OMX_IndexParamPortDefinition, &portDefinition);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
}

static OMX_BUFFERHEADERTYPE* component_allocate_buffer(OMX_HANDLETYPE handle, int port, ssize_t size)
{
    printf("%s:%d\n", __func__, __LINE__);
    OMX_ERRORTYPE result;
    OMX_BUFFERHEADERTYPE* buffer;
    result = OMX_AllocateBuffer(handle, &buffer, port, NULL, size);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
    buffer->pAppPrivate = (OMX_PTR) BUFFER_UNUSED;
    return buffer;
}

static OMX_BUFFERHEADERTYPE* component_use_buffer(OMX_HANDLETYPE handle, int port, uint8_t *data, size_t size)
{
    printf("%s:%d\n", __func__, __LINE__);
    OMX_ERRORTYPE result;
    OMX_BUFFERHEADERTYPE* buffer;
    result = OMX_UseBuffer(handle, &buffer, port, NULL, size, (OMX_U8*)data);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
    buffer->pAppPrivate = (OMX_PTR) BUFFER_UNUSED;
    return buffer;
}

static void component_push_output_buffer(OMX_HANDLETYPE handle, OMX_BUFFERHEADERTYPE* buffer)
{
    printf("%s:%d push output buffer %p\n", __func__, __LINE__, buffer);
    OMX_ERRORTYPE result;
    buffer->pAppPrivate = (OMX_PTR) BUFFER_USED;
    result = OMX_FillThisBuffer(handle, buffer);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
}

static void component_set_tunnel(OMX_HANDLETYPE src, int portSrc, OMX_HANDLETYPE dst, int portDst)
{
    printf("%s:%d\n", __func__, __LINE__);
    OMX_ERRORTYPE result;
    result = OMX_SetupTunnel(src, portSrc, dst, portDst);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
}

int main(int argc, char **argv)
{
    if (argc < 2) {
        fprintf(stderr, "Usage: %s JPEGFILE\n", argv[0]);
        return 2;
    }

    char* filename = argv[1];
    int fd = open(filename, O_RDONLY);

    OMX_ERRORTYPE result;

    bcm_host_init();
    OMX_Init();

    OMX_HANDLETYPE decoderHandle, resizerHandle;
    int decoderInPort, decoderOutPort;
    int resizerInPort, resizerOutPort;

    OMX_CALLBACKTYPE callbacks = OMX_CALLBACKTYPE();
    callbacks.EventHandler = event_handler;
    callbacks.EmptyBufferDone = empty_buffer_done;
    callbacks.FillBufferDone = fill_buffer_done;

    component_init_handle(&decoderHandle, (char*)"OMX.broadcom.image_decode", &callbacks);
    component_get_ports(decoderHandle, &decoderInPort, &decoderOutPort);
    component_disable_port(decoderHandle, decoderInPort);
    component_disable_port(decoderHandle, decoderOutPort);

    component_set_state(decoderHandle, OMX_StateIdle);

    component_setup_input_port(decoderHandle, decoderInPort);

    int inputBuffersCount, inputBuffersSize;
    component_get_buffering_parameters(decoderHandle, decoderInPort, &inputBuffersCount, &inputBuffersSize, NULL, NULL);

    component_enable_port(decoderHandle, decoderInPort);

    OMX_BUFFERHEADERTYPE* inputBufferHeaders[MAX_BUFFERS] = { 0 };
    OMX_BUFFERHEADERTYPE* outputBufferHeaders[MAX_BUFFERS] = { 0 };

    for(int i=0; i<inputBuffersCount; i++)
        inputBufferHeaders[i] = component_allocate_buffer(decoderHandle, decoderInPort, inputBuffersSize);

    printf("%s:%d\n", __func__, __LINE__);

    component_set_state(decoderHandle, OMX_StateExecuting);

    bool allDataReceived = false;
    bool portSettingsChanged = false;
    while (!allDataReceived || !portSettingsChanged) {
        printf("%s:%d\n", __func__, __LINE__);
        if (!portSettingsChanged && test_event(EVENT_PORT_SETTINGS_CHANGED))
        {
            printf("%s:%d set up format conversion tunnel\n", __func__, __LINE__);

            component_init_handle(&resizerHandle, (char*)"OMX.broadcom.resize", &callbacks);
            component_get_ports(resizerHandle, &resizerInPort, &resizerOutPort);
            component_disable_port(resizerHandle, resizerInPort);
            component_disable_port(resizerHandle, resizerOutPort);

            component_set_tunnel(decoderHandle, decoderOutPort, resizerHandle, resizerInPort);

            component_enable_port(resizerHandle, resizerInPort);
            component_enable_port(decoderHandle, decoderOutPort);

            component_set_state(resizerHandle, OMX_StateIdle);
            component_set_state(resizerHandle, OMX_StateExecuting);

            component_setup_output_port(resizerHandle, resizerOutPort);

            int outputBuffersCount, outputBuffersSize;
            int slice, stride;
            component_get_buffering_parameters(resizerHandle, resizerOutPort, &outputBuffersCount, &outputBuffersSize, &slice, &stride);

            outputBuffersCount = MAX(outputBuffersCount, 2);
            component_set_buffering_parameters(resizerHandle, resizerOutPort, outputBuffersCount, outputBuffersSize, slice, stride);

            component_enable_port(resizerHandle, resizerOutPort);

            for(int i=0; i<outputBuffersCount; i++) {
                outputBufferHeaders[i] = component_allocate_buffer(resizerHandle, resizerOutPort, outputBuffersSize);
                component_push_output_buffer(resizerHandle, outputBufferHeaders[i]);
            }

            portSettingsChanged = true;
        }

        if (test_event(EVENT_BUFFER_FILLED)) {
            printf("%s:%d pushing output buffers\n", __func__, __LINE__);
            while (OMX_BUFFERHEADERTYPE* outputBufferHeader = get_free_buffer(outputBufferHeaders))
                component_push_output_buffer(resizerHandle, outputBufferHeader);
            printf("%s:%d\n", __func__, __LINE__);
        }

        printf("%s:%d wait for free input buffers\n", __func__, __LINE__);
        OMX_BUFFERHEADERTYPE* inputBufferHeader = get_free_buffer(inputBufferHeaders);
        printf("%s:%d\n", __func__, __LINE__);
        if (inputBufferHeader == NULL) {
            printf("%s:%d insufficient buffering\n", __func__, __LINE__);
            exit(1);
        }

        inputBufferHeader->nFilledLen = read(fd, inputBufferHeader->pBuffer, inputBufferHeader->nAllocLen);
        allDataReceived = inputBufferHeader->nFilledLen == 0;
        if (allDataReceived)
            inputBufferHeader->nFlags = OMX_BUFFERFLAG_EOS;

        printf("%s:%d submit buffer %p with %d bytes, flags %x\n", __func__, __LINE__, inputBufferHeader, inputBufferHeader->nFilledLen, inputBufferHeader->nFlags);
        result = OMX_EmptyThisBuffer(decoderHandle, inputBufferHeader);
        printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
    }

    while(!test_event(EVENT_BUFFER_FLAG)) {
        printf("%s:%d pushing output buffers\n", __func__, __LINE__);
        while (OMX_BUFFERHEADERTYPE* outputBufferHeader = get_free_buffer(outputBufferHeaders))
            component_push_output_buffer(resizerHandle, outputBufferHeader);
        printf("%s:%d\n", __func__, __LINE__);
    }

    printf("%s:%d\n", __func__, __LINE__);

    OMX_PARAM_PORTDEFINITIONTYPE outPortDefinition = OMX_PARAM_PORTDEFINITIONTYPE();
    outPortDefinition.nSize = sizeof(OMX_PARAM_PORTDEFINITIONTYPE);
    outPortDefinition.nVersion.nVersion = OMX_VERSION;
    outPortDefinition.nPortIndex = resizerOutPort;

    result = OMX_GetParameter(resizerHandle, OMX_IndexParamPortDefinition, &outPortDefinition);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));

    if (outPortDefinition.format.image.eColorFormat != OMX_COLOR_Format32bitARGB8888)
        printf("%s:%d wrong format, got %d, expected %d\n", __func__, __LINE__, outPortDefinition.format.image.eColorFormat, OMX_COLOR_Format32bitARGB8888);

    cairo_status_t s;
    cairo_format_t format = CAIRO_FORMAT_RGB24;
    printf("%s:%d image size %dx%d stride %d\n", __func__, __LINE__, outPortDefinition.format.image.nFrameWidth, outPortDefinition.format.image.nFrameHeight, outPortDefinition.format.image.nStride);
    cairo_surface_t* surface = cairo_image_surface_create_for_data(decoded_data, format,
                                                                   outPortDefinition.format.image.nFrameWidth,
                                                                   outPortDefinition.format.image.nFrameHeight,
                                                                   outPortDefinition.format.image.nStride);
    s = cairo_surface_status(surface);
    printf("%s:%d → %s\n", __func__, __LINE__, cairo_status_to_string(s));

    char output[1024];
    sprintf(output, "%s.png", filename);
    printf("%s:%d output to %s\n", __func__, __LINE__, output);

    s = cairo_surface_write_to_png(surface, output);
    printf("%s:%d → %s\n", __func__, __LINE__, cairo_status_to_string(s));

    /* release resources */
    result = OMX_SendCommand(decoderHandle, OMX_CommandFlush, decoderOutPort, NULL);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
    result = OMX_SendCommand(resizerHandle, OMX_CommandFlush, resizerInPort, NULL);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));

    result = OMX_SendCommand(decoderHandle, OMX_CommandStateSet, OMX_StateIdle, NULL);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));

    result = OMX_SendCommand(resizerHandle, OMX_CommandStateSet, OMX_StateIdle, NULL);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));

    result = OMX_SendCommand(decoderHandle, OMX_CommandStateSet, OMX_StateLoaded, NULL);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));

    result = OMX_SendCommand(resizerHandle, OMX_CommandStateSet, OMX_StateLoaded, NULL);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));

    for(int i=0; i<MAX_BUFFERS; i++) {
        if (!inputBufferHeaders[i])
            continue;
        result = OMX_FreeBuffer(decoderHandle, decoderInPort, inputBufferHeaders[i]);
        printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
    }

    result = OMX_SendCommand(decoderHandle, OMX_CommandPortDisable, decoderInPort, NULL);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));

    result = OMX_SendCommand(resizerHandle, OMX_CommandPortDisable, resizerOutPort, NULL);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));

    for(int i=0; i<MAX_BUFFERS; i++) {
        if (!outputBufferHeaders[i])
            continue;
        result = OMX_FreeBuffer(resizerHandle, resizerOutPort, outputBufferHeaders[i]);
        printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
    }

    result = OMX_SendCommand(decoderHandle, OMX_CommandPortDisable, decoderOutPort, NULL);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));

    result = OMX_SendCommand(resizerHandle, OMX_CommandPortDisable, resizerInPort, NULL);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));

    result = OMX_SetupTunnel(decoderHandle, decoderOutPort, NULL, 0);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
    result = OMX_SetupTunnel(resizerHandle, resizerInPort, NULL, 0);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));

    result = OMX_FreeHandle(decoderHandle);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));
    result = OMX_FreeHandle(resizerHandle);
    printf("%s:%d → %s\n", __func__, __LINE__, omxerrorstr(result));

    OMX_Deinit();
    return 0;
}
